package com.tech.raw.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	
	private String text;
	
	private String qrcode;
	
	private String header1;
	private String header2;
	private String header3;
	private String header4;
	private String header5;
	
	private String[] bodyItem;
	
	private String bodyItem1;
	private String bodyItem2;
	private String bodyItem3;
	private String bodyItem4;
	
	private String[] bodyTax1;
	
	private String footer1;
	private String footer2;
	private String footer3;
	private String footer4;
	private String footer5;
	private String footer6;
	
	
	private boolean cut;
	private boolean copy;
	private boolean invoiceData;
}
