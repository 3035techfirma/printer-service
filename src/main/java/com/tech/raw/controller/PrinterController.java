package com.tech.raw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tech.raw.dto.MessageDTO;
import com.tech.raw.service.PrinterService;

@RestController
@RequestMapping(value = "/printer")
public class PrinterController {

	
	@Autowired
	private PrinterService printerService;
	
	@PostMapping("/send")
    public void send(@RequestBody MessageDTO messageDTO){
		printerService.send(messageDTO);
	}
}
