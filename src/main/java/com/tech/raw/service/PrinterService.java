package com.tech.raw.service;

import java.io.IOException;

import javax.print.PrintService;

import org.springframework.stereotype.Service;

import com.tech.raw.dto.MessageDTO;

import escpos.EscPos;
import escpos.EscPos.CutMode;
import escpos.EscPosConst;
import escpos.Style;
import escpos.barcode.QRCode;
import lombok.extern.log4j.Log4j2;
import output.PrinterOutputStream;

@Log4j2
@Service
public class PrinterService {

//	
//	public void send(MessageDTO messageDTO) {
//		PrintService printService = PrinterOutputStream.getPrintServiceByName(messageDTO.getName());
//        try {
//            PrinterOutputStream printerOutputStream = new PrinterOutputStream(printService);
//            EscPos escpos = new EscPos(printerOutputStream);
//            escpos.feed(5);
//            escpos.writeLF(messageDTO.getText());
//            escpos.feed(5);
//            if(messageDTO.isCut()) {
//            	escpos.cut(CutMode.PART);
//            }
//            escpos.close();
//            
//        } catch (IOException ex) {
//            log.info("MESSAGE DID NOT PRINTED", ex.getMessage());
//        }
//	}

	private String LINE = "------------------------------------------------";

	public void send(MessageDTO messageDTO) {
		PrintService printService = PrinterOutputStream.getPrintServiceByName(messageDTO.getName());
		try {
			PrinterOutputStream printerOutputStream = new PrinterOutputStream(printService);
			EscPos escpos = new EscPos(printerOutputStream);
			escpos.feed(5);

			Style title = new Style().setFontSize(Style.FontSize._2, Style.FontSize._2)
					.setJustification(EscPosConst.Justification.Center);

			Style bold = new Style().setBold(true).setJustification(EscPosConst.Justification.Center);

			Style center = new Style().setJustification(EscPosConst.Justification.Center);

			escpos.writeLF(title, messageDTO.getHeader1()).feed(1).writeLF(center, messageDTO.getHeader2())
					.writeLF(center, messageDTO.getHeader3()).writeLF(center, messageDTO.getHeader4())
					.writeLF(center, messageDTO.getHeader5()).writeLF(LINE);

			escpos.writeLF(center, "DANFE NFC-e - Documento Auxiliar")
					.writeLF(center, "da Nota Fiscal de Consumidor Eletronica")
					.writeLF(center, "Nao permite aproveitamento de credito de ICMS").writeLF(LINE);

			if (null != messageDTO.getBodyItem()) {
				escpos.writeLF("# |    CODIGO   |      DESC     | QTD | VL TOTAL").writeLF(LINE);
				int count = 0;
				for (String item : messageDTO.getBodyItem()) {
					count++;
					escpos.writeLF(count + "  " + item);
				}
			}
			escpos.writeLF(LINE);
			escpos.writeLF(messageDTO.getBodyItem1());
			escpos.writeLF(messageDTO.getBodyItem2());
			escpos.writeLF(messageDTO.getBodyItem3());
			escpos.writeLF(messageDTO.getBodyItem4());
			escpos.writeLF(LINE);

			if (null != messageDTO.getBodyTax1()) {
				escpos.writeLF("Trib Aprox R$:" + messageDTO.getBodyTax1()[0].toString() + " e "
						+ messageDTO.getBodyTax1()[1].toString() + " Estadual");
				escpos.writeLF(LINE);
			}
			if (messageDTO.isInvoiceData()) {
				escpos.writeLF(center, messageDTO.getFooter1());
				escpos.writeLF(center, messageDTO.getFooter2());
				escpos.writeLF(bold, "Via Consumidor");
				escpos.writeLF(center, "Consulte pela Chave de Acesso em");
				escpos.writeLF(center, "https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx");
				escpos.writeLF(bold, "CHAVE DE ACESSO");
				escpos.writeLF(bold, messageDTO.getFooter3());
				escpos.writeLF(LINE);
				escpos.writeLF(center, messageDTO.getFooter4());
				escpos.writeLF(LINE);
				escpos.writeLF(bold, "Consulta via leitor de QR Code");
				QRCode qrcode = new QRCode();
				qrcode.setSize(7);
				qrcode.setJustification(EscPosConst.Justification.Center);
				escpos.feed(1);
				escpos.write(qrcode, messageDTO.getQrcode());
				escpos.feed(1);
				escpos.writeLF(bold, messageDTO.getFooter5());
				escpos.writeLF(bold, messageDTO.getFooter6());
			}

			escpos.feed(8);

			if (messageDTO.isCut()) {
				escpos.cut(CutMode.PART);
			}

			if (messageDTO.isCopy()) {
				escpos.writeLF(center, "Cliente: " + messageDTO.getFooter4());
				escpos.writeLF(LINE);
				if (null != messageDTO.getBodyItem()) {
					escpos.writeLF("# |    CODIGO   |      DESC     | QTD | VL TOTAL").writeLF(LINE);
					int count = 0;
					for (String item : messageDTO.getBodyItem()) {
						count++;
						escpos.writeLF(count + "  " + item);
					}
					escpos.writeLF(LINE);
					escpos.writeLF(messageDTO.getBodyItem2());
				}
				escpos.writeLF(center, messageDTO.getFooter1());
				escpos.writeLF(center, messageDTO.getFooter2());
				escpos.writeLF(bold, "Via Estabelecimento");
				escpos.feed(4);
				escpos.cut(CutMode.PART);
			}
			escpos.close();

		} catch (IOException ex) {
			log.info("MESSAGE DID NOT PRINTED", ex.getMessage());
		}
	}

}
